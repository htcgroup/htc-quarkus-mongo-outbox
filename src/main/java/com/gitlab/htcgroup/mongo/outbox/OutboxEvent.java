package com.gitlab.htcgroup.mongo.outbox;

import com.fasterxml.jackson.databind.JsonNode;
import org.bson.types.ObjectId;

import java.time.Instant;

public class OutboxEvent {
    
    private ObjectId id;
    
    private String aggregateId;
    
    private String aggregateType;
    
    private String type;
    
    private Instant timestamp;
    
    private JsonNode payload;
    
    protected OutboxEvent() {
    }
    
    public OutboxEvent(String aggregateId, String aggregateType, String type, Instant timestamp, JsonNode payload) {
        this.aggregateId = aggregateId;
        this.aggregateType = aggregateType;
        this.type = type;
        this.timestamp = timestamp;
        this.payload = payload;
    }
    
    public ObjectId getId() {
        return id;
    }
    
    public void setId(ObjectId id) {
        this.id = id;
    }
    
    public String getAggregateId() {
        return aggregateId;
    }
    
    public void setAggregateId(String aggregateId) {
        this.aggregateId = aggregateId;
    }
    
    public String getAggregateType() {
        return aggregateType;
    }
    
    public void setAggregateType(String aggregateType) {
        this.aggregateType = aggregateType;
    }
    
    public String getType() {
        return type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public Instant getTimestamp() {
        return timestamp;
    }
    
    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }
    
    public JsonNode getPayload() {
        return payload;
    }
    
    public void setPayload(JsonNode payload) {
        this.payload = payload;
    }
}
