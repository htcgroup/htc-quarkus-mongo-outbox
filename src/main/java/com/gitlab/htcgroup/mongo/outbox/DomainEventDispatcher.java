package com.gitlab.htcgroup.mongo.outbox;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.MongoClient;
import org.bson.Document;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import java.util.Map;

@ApplicationScoped
public class DomainEventDispatcher {
    
    private static final Logger LOG = LoggerFactory.getLogger(DomainEventDispatcher.class);
    
    private final ObjectMapper mapper = new ObjectMapper();
    
    @Inject
    @ConfigProperty(name = "quarkus.mongodb.database")
    String database;
    
    @Inject
    MongoClient mongoClient;
    
    public void onDomainEvent(@Observes DomainEvent event) {
        LOG.debug("A domain event was found for type {}", event.getType());
        
        final OutboxEvent outboxEvent = getDataFromEvent(event);
        Map<String, Object> outboxMap = mapper.convertValue(outboxEvent.getPayload(), new TypeReference<>() {});
    
        Document outboxDocument = new Document()
            .append("aggregate_type", outboxEvent.getAggregateType())
            .append("aggregate_id", outboxEvent.getAggregateId())
            .append("event_type", outboxEvent.getType())
            .append("payload", outboxMap)
            .append("timestamp", outboxEvent.getTimestamp().toString());
        
        mongoClient.getDatabase(database)
            .getCollection("outboxevent")
            .insertOne(outboxDocument);
    }
    
    private OutboxEvent getDataFromEvent(DomainEvent event) {
        return new OutboxEvent(
            event.getAggregateId(),
            event.getAggregateType(),
            event.getType(),
            event.getTimestamp(),
            event.getPayload()
        );
    }
}
